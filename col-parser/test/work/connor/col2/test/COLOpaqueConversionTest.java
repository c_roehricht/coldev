package work.connor.col2.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import work.connor.col2.COL.Expression;
import work.connor.col2.COL.Expression.Structure;
import work.connor.col2.COL.Expression.Number;
import work.connor.col2.COL.Expression.Data;
import work.connor.col2.COL.Character.Symbol;
import work.connor.col2.Utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class COLOpaqueConversionTest {
    private static class TestVector {
        final String name;
        final Expression expression;
        final byte[] octets;
        TestVector(String name, Expression expression) {
            this.name = name;
            this.expression = expression;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            expression.toOctets(out::write);
            this.octets = out.toByteArray();
        }
    }

    private static final TestVector complex1 = new TestVector("Complex1",
            new Structure(Symbol.and,
                    new Structure(Symbol.map,
                            new Structure(Symbol.query,
                                    new Structure("·wer"),
                                    new Structure("·andern"),
                                    new Structure(Symbol.multiply,
                                            new Number(1),
                                            new Structure("·bratwurst")),
                                    new Structure("·brāt")),
                            new Structure(Symbol.query,
                                    new Structure("·hat"),
                                    new Structure(Symbol.multiply,
                                            new Number(1),
                                            new Structure("·bratwurst·brat·gerāt")))),
                    new Data("000102030405060708090A0B0C0D0E0F" +
                            "101112131415161718191A1B1C1D1E1F" +
                            "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF")));

    private static final TestVector complex2;
    static {
        TestVector complex = complex1;
        for (int i = 0; i < 8; i++) {
            complex = new TestVector("Complex2", new Structure(Symbol.and, complex.expression, complex.expression));
        }
        complex2 = complex;
    }

    private static final java.util.List<TestVector> testVectors = Arrays.asList(complex1, complex2);

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> testVectors() {
        return testVectors.stream().map(tv -> new Object[] {tv.name, tv.expression, tv.octets}).collect(Collectors.toList());
    }

    private Expression e;
    private byte[] octets;

    public COLOpaqueConversionTest(String name, Expression e, byte[] octets) {
        this.e = e;
        this.octets = octets;
    }

    @Test
    public void write() throws Exception {
        // Not an actual test, just checking the performance
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        e.toOctets(out::write);
        out.toByteArray();
        System.out.println(out.size());
    }

    @Test
    public void parse() throws Exception {
        // Not an actual test, just checking the performance
        ByteArrayInputStream in = new ByteArrayInputStream(octets);
        Expression eNew = Expression.fromOctets(() -> (byte) in.read());
    }

    @Test
    public void writeAndParse() throws Exception {
        assertEquals(e, Utility.writeAndParse(e, Expression::fromOctets, Expression::toOctets));
    }

    @Test
    public void parseAndWrite() throws Exception {
        // Not an actual test, just checking the performance
        ByteArrayInputStream in = new ByteArrayInputStream(octets);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Utility.parseAndWrite(() -> (byte) in.read(), out::write,
                Expression::fromOctets, Expression::toOctets);
        assertArrayEquals(octets, out.toByteArray());
    }
}