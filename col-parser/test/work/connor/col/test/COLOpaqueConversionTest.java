package work.connor.col.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import work.connor.col.COL.Character;
import work.connor.col.COL.*;
import work.connor.col.COL.Number;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class COLOpaqueConversionTest {
    private static class TestVector {
        public final String name;
        public final Expression expression;
        public TestVector(String name, Expression expression) {
            this.name = name;
            this.expression = expression;
        }
    }

    private static final TestVector complex1 = new TestVector("Complex1",
            new List(new Operation(Character.OperatorMap, new Null()),
                     new Operation(Character.OperatorMap),
                     new Operation(Character.OperatorMap, new Operation(Character.OperatorMap, new List(new Number(0)))),
                     new List(new Number(10000000), new Name("··bratwurst·brat·gerāt"), new Name("·")),
                     new Entity(Character.EntityTrue),
                     new Data("000102030405060708090A0B0C0D0E0F" +
                              "101112131415161718191A1B1C1D1E1F" +
                              "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF")
            ));
    private static final TestVector complex2;
    static {
        TestVector complex = complex1;
        for (int i = 0; i < 15; i++) {
            complex = new TestVector("Complex2", new List(complex.expression, complex.expression));
        }
        complex2 = complex;
    }

    private static final java.util.List<TestVector> testVectors = Arrays.asList(complex1, complex2);

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> testVectors() {
        return testVectors.stream().map(tv -> new Object[] {tv.name, tv.expression}).collect(Collectors.toList());
    }

    private Expression e;

    public COLOpaqueConversionTest(String name, Expression e) {
        this.e = e;
    }

    @Test
    public void writeAndParse() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        e.write(out);
        System.out.println(out.size());
        Expression eNew = Expression.parse(ByteBuffer.wrap(out.toByteArray()));
        assertEquals(e, eNew);
    }
}