package work.connor.col.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import work.connor.col.COL.Expression;
import work.connor.col.COL.Character;
import work.connor.col.COL.Name;
import work.connor.col.COL.Number;
import work.connor.col.COL.Null;
import work.connor.col.COL.Entity;
import work.connor.col.COL.List;
import work.connor.col.COL.Operation;
import work.connor.col.COL.Data;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class COLKnownConversionTest {
    private static class TestVector {
        public final byte[] octets;
        public final Expression expression;
        public final String unicode;
        public TestVector(byte[] octets, Expression expression, String unicode) {
            this.octets = octets;
            this.expression = expression;
            this.unicode = unicode;
        }
    }

    private static final TestVector name1 = new TestVector(
            new byte[] {Character.Syllabifier.octet, Character.LetterT.octet, Character.LetterE.octet,  Character.LetterS.octet, Character.LetterT.octet,
                    Character.Syllabifier.octet, Character.LetterN.octet, Character.LetterAX.octet, Character.LetterM.octet, Character.LetterE.octet},
            new Name(new byte[] {Character.LetterT.octet, Character.LetterE.octet,  Character.LetterS.octet, Character.LetterT.octet,
                    Character.Syllabifier.octet, Character.LetterN.octet, Character.LetterAX.octet, Character.LetterM.octet, Character.LetterE.octet}),
            "·test·nāme");
    private static final TestVector number1 = new TestVector(
            new byte[] {Character.DigitFour.octet, Character.DigitTwo.octet, Character.DigitA.octet},
            new Number(0x42A),
            "42A");
    private static final TestVector null1 = new TestVector(
            new byte[] {},
            new Null(),
            "");
    private static final TestVector entity1 = new TestVector(
            new byte[] {Character.EntityTrue.octet},
            new Entity(Character.EntityTrue.octet),
            "⊤");
    private static final java.util.List<TestVector> list1Elements = Arrays.asList(name1, number1, entity1);
    private static final byte[] list1Octets;
    private static final String list1Unicode;
    static {
        StringBuilder sb = new StringBuilder();
        ArrayList<Byte> octets = new ArrayList<>();
        for (TestVector tv : list1Elements) {
            octets.add(Character.ListSeparator.octet);
            for (byte octet : tv.octets) octets.add(octet);
            sb.append("■");
            sb.append(tv.unicode);
        }
        list1Octets = new byte[octets.size()];
        for (int i = 0; i < octets.size(); i++) list1Octets[i] = octets.get(i);
        list1Unicode = sb.toString();
    }
    private static final TestVector list1 = new TestVector(
            list1Octets,
            new List(list1Elements.stream().map(tv -> tv.expression).collect(Collectors.toList())),
            list1Unicode);
    private static final TestVector operator1 = new TestVector(
            new byte[] {Character.OperatorMap.octet},
            new Operation(Character.OperatorMap.octet, Collections.emptyList()),
            "→");
    private static final TestVector operator2 = new TestVector(
            new byte[] {Character.OperatorMap.octet, Character.ListSeparator.octet, Character.DigitZero.octet,
                    Character.ListSeparator.octet, Character.EntityFalse.octet},
            new Operation(Character.OperatorMap.octet, Arrays.asList(new Number(0), new Entity(Character.EntityFalse.octet))),
            "→■0■⊥");
    private static final TestVector data1 = new TestVector(
            new byte[] {Character.DataBegin.octet,
                    Character.ListSeparator.octet, Character.EntityTrue.octet, Character.DigitZero.octet,
                    Character.LetterA.octet, Character.Syllabifier.octet, Character.DataEnd.octet, Character.DataEnd.octet,
                    Character.DataEnd.octet, Character.DataEnd.octet, Character.OperatorMap.octet,
                    Character.DataEnd.octet},
            new Data(new byte[] {
                    Character.ListSeparator.octet, Character.EntityTrue.octet, Character.DigitZero.octet,
                    Character.LetterA.octet, Character.Syllabifier.octet, Character.DataEnd.octet, Character.DataEnd.octet,
                    Character.OperatorMap.octet}),
            "⟨■⊤0a·⟩⟩⟩⟩→⟩");

    private static final java.util.List<TestVector> testVectors = Arrays.asList(
            name1, number1, null1, entity1, list1, operator1, operator2, data1);

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> testVectors() {
        return testVectors.stream().map(tv -> new Object[] {tv.unicode, tv}).collect(Collectors.toList());
    }

    private TestVector tv;

    public COLKnownConversionTest(String name, TestVector tv) {
        this.tv = tv;
    }

    @Test
    public void parseValidSingleExpression() throws Exception {
        Expression e = Expression.parse(ByteBuffer.wrap(tv.octets));
        assertEquals(tv.expression, e);
    }

    @Test
    public void parseValidSingleExpressionFromUnicode() throws Exception {
        Expression e = Expression.parse(ByteBuffer.wrap(Character.octetsFromUnicode(tv.unicode)));
        assertEquals(tv.expression, e);
    }

    @Test
    public void writeSingleExpression() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        tv.expression.write(out);
        assertArrayEquals(tv.octets, out.toByteArray());
    }

    @Test
    public void writeSingleExpressionToUnicode() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        tv.expression.write(out);
        assertEquals(tv.unicode, Character.unicodeFromOctets(out.toByteArray()));
    }
}