package work.connor.col2;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @// TODO: 16.08.17 Numbers shall not have leading zeroes
 * @// TODO: 16.08.17 Token parsing shall check more characters than the first one
 * @// TODO: 18.08.17 Only accept even number of nibbles?
 */
public class COL {
    /** Single COL character. Each character corresponds to a unique octet value (for X2M interfaces)
     *  and a unique Unicode code point (for X2H interfaces). All instances are immutable, available at initialization
     *  and managed by the class (unique references). All subclasses are nested.
     *  This class also contains convenience methods for efficient conversion. */
    public static class Character {
        /** Octet value (X2M form) */
        public byte octet;
        /** Unicode code point (X2H form) */
        public int codePoint;
        /** Look-up table for characters by octet value */
        public static final Map<Byte, Character> ofOctet = new HashMap<>();
        /** Look-up table for characters by Unicode code point */
        public static final Map<Integer, Character> ofCodePoint = new HashMap<>();

        static {
            Digit.init();
            Letter.init();
            Nibble.init();
            Symbol.init();
        }

        static Character exists(Character character) {
            if(ofOctet.put(character.octet, character) != null) throw new IllegalArgumentException("Duplicate character");
            if(ofCodePoint.put(character.codePoint, character) != null) throw new IllegalArgumentException("Duplicate character");
            return character;
        }

        protected Character(byte octet, int codePoint) {
            this.octet = octet;
            this.codePoint = codePoint;
        }
        protected Character(byte octet, String codePointString) {
            this(octet, codePointString.codePoints().findFirst().getAsInt());
        }

        @Override
        public String toString() {
            return new String(java.lang.Character.toChars(codePoint));
        }

        /** Character subtype for ·col·digit⋯s. Digits are bijectively mapped to integers in [0,F].
         * Their code points are uppercase hexadecimal digits. */
        public static class Digit extends Character {
            static void init() { }
            /** Mapped integer */
            public final int value;
            /** Look-up table for digits by mapped integer */
            public static Map<Integer, Digit> ofValue = new HashMap<>();
            static Digit exists(Digit digit) {
                if(ofValue.put(digit.value, digit) != null) throw new IllegalArgumentException("Duplicate digit");
                Character.exists(digit);
                return digit;
            }

            public static final Digit zero  = Digit.exists(new Digit((byte) 0x00, "0"));
            public static final Digit one   = Digit.exists(new Digit((byte) 0x01, "1"));
            public static final Digit two   = Digit.exists(new Digit((byte) 0x02, "2"));
            public static final Digit three = Digit.exists(new Digit((byte) 0x03, "3"));
            public static final Digit four  = Digit.exists(new Digit((byte) 0x04, "4"));
            public static final Digit five  = Digit.exists(new Digit((byte) 0x05, "5"));
            public static final Digit six   = Digit.exists(new Digit((byte) 0x06, "6"));
            public static final Digit seven = Digit.exists(new Digit((byte) 0x07, "7"));
            public static final Digit eight = Digit.exists(new Digit((byte) 0x08, "8"));
            public static final Digit nine  = Digit.exists(new Digit((byte) 0x09, "9"));
            public static final Digit a     = Digit.exists(new Digit((byte) 0x0A, "A"));
            public static final Digit b     = Digit.exists(new Digit((byte) 0x0B, "B"));
            public static final Digit c     = Digit.exists(new Digit((byte) 0x0C, "C"));
            public static final Digit d     = Digit.exists(new Digit((byte) 0x0D, "D"));
            public static final Digit e     = Digit.exists(new Digit((byte) 0x0E, "E"));
            public static final Digit f     = Digit.exists(new Digit((byte) 0x0F, "F"));

            protected Digit(byte octet, String codePointString, int value) {
                super(octet, codePointString);
                this.value = value;
            }

            protected Digit(byte octet, String codePointString) {
                this(octet, codePointString, octet & 0xFF);
            }
        }

        /** Character subtype for ·col·letter⋯s.
         * Their code points are lowercase latin letters, 5 modified vowels and the ·syllabifier⋯. */
        public static class Letter extends Character {
            static void init() {}

            static Letter exists(Letter letter) {
                Character.exists(letter);
                return letter;
            }

            public static final Letter a  = Letter.exists(new Letter((byte) 0xD0, "a"));
            public static final Letter b  = Letter.exists(new Letter((byte) 0xD1, "b"));
            public static final Letter c  = Letter.exists(new Letter((byte) 0xD2, "c"));
            public static final Letter d  = Letter.exists(new Letter((byte) 0xD3, "d"));
            public static final Letter e  = Letter.exists(new Letter((byte) 0xD4, "e"));
            public static final Letter f  = Letter.exists(new Letter((byte) 0xD5, "f"));
            public static final Letter g  = Letter.exists(new Letter((byte) 0xD6, "g"));
            public static final Letter h  = Letter.exists(new Letter((byte) 0xD7, "h"));
            public static final Letter i  = Letter.exists(new Letter((byte) 0xD8, "i"));
            public static final Letter j  = Letter.exists(new Letter((byte) 0xD9, "j"));
            public static final Letter k  = Letter.exists(new Letter((byte) 0xDA, "k"));
            public static final Letter l  = Letter.exists(new Letter((byte) 0xDB, "l"));
            public static final Letter m  = Letter.exists(new Letter((byte) 0xDC, "m"));
            public static final Letter n  = Letter.exists(new Letter((byte) 0xDD, "n"));
            public static final Letter o  = Letter.exists(new Letter((byte) 0xDE, "o"));
            public static final Letter p  = Letter.exists(new Letter((byte) 0xDF, "p"));
            public static final Letter q  = Letter.exists(new Letter((byte) 0xE0, "q"));
            public static final Letter r  = Letter.exists(new Letter((byte) 0xE1, "r"));
            public static final Letter s  = Letter.exists(new Letter((byte) 0xE2, "s"));
            public static final Letter t  = Letter.exists(new Letter((byte) 0xE3, "t"));
            public static final Letter u  = Letter.exists(new Letter((byte) 0xE4, "u"));
            public static final Letter v  = Letter.exists(new Letter((byte) 0xE5, "v"));
            public static final Letter w  = Letter.exists(new Letter((byte) 0xE6, "w"));
            public static final Letter x  = Letter.exists(new Letter((byte) 0xE7, "x"));
            public static final Letter y  = Letter.exists(new Letter((byte) 0xE8, "y"));
            public static final Letter z  = Letter.exists(new Letter((byte) 0xE9, "z"));
            public static final Letter aX  = Letter.exists(new Letter((byte) 0xEA, "ā"));
            public static final Letter eX  = Letter.exists(new Letter((byte) 0xEB, "ē"));
            public static final Letter iX  = Letter.exists(new Letter((byte) 0xEC, "ī"));
            public static final Letter oX  = Letter.exists(new Letter((byte) 0xED, "ō"));
            public static final Letter uX  = Letter.exists(new Letter((byte) 0xEE, "ū"));
            public static final Letter syllabifier  = Letter.exists(new Letter((byte) 0xEF, "·"));

            protected Letter(byte octet, String codePointString) {
                super(octet, codePointString);
            }
        }

        /** Character subtype for ·col·nibble⋯s. Nibbles are bijectively mapped to integers in [0,F].
         * Their code points are highlighted uppercase hexadecimal digits. */
        public static class Nibble extends Character {
            static void init() { }
            /** Mapped integer */
            public final int value;
            /** Look-up table for nibbles by mapped integer */
            public static Map<Integer, Nibble> ofValue = new HashMap<>();
            static Nibble exists(Nibble nibble) {
                if (ofValue.put(nibble.value, nibble) != null) throw new IllegalArgumentException("Duplicate nibble");
                Character.exists(nibble);
                return nibble;
            }

            public static final Nibble zero  = Nibble.exists(new Nibble((byte) 0xF0, "０"));
            public static final Nibble one   = Nibble.exists(new Nibble((byte) 0xF1, "１"));
            public static final Nibble two   = Nibble.exists(new Nibble((byte) 0xF2, "２"));
            public static final Nibble three = Nibble.exists(new Nibble((byte) 0xF3, "３"));
            public static final Nibble four  = Nibble.exists(new Nibble((byte) 0xF4, "４"));
            public static final Nibble five  = Nibble.exists(new Nibble((byte) 0xF5, "５"));
            public static final Nibble six   = Nibble.exists(new Nibble((byte) 0xF6, "６"));
            public static final Nibble seven = Nibble.exists(new Nibble((byte) 0xF7, "７"));
            public static final Nibble eight = Nibble.exists(new Nibble((byte) 0xF8, "８"));
            public static final Nibble nine  = Nibble.exists(new Nibble((byte) 0xF9, "９"));
            public static final Nibble a     = Nibble.exists(new Nibble((byte) 0xFA, "Ａ"));
            public static final Nibble b     = Nibble.exists(new Nibble((byte) 0xFB, "Ｂ"));
            public static final Nibble c     = Nibble.exists(new Nibble((byte) 0xFC, "Ｃ"));
            public static final Nibble d     = Nibble.exists(new Nibble((byte) 0xFD, "Ｄ"));
            public static final Nibble e     = Nibble.exists(new Nibble((byte) 0xFE, "Ｅ"));
            public static final Nibble f     = Nibble.exists(new Nibble((byte) 0xFF, "Ｆ"));

            protected Nibble(byte octet, String codePointString) {
                this(octet, codePointString, (octet & 0xFF) - 0xF0);
            }
            protected Nibble(byte octet, String codePointString, int value) {
                super(octet, codePointString);
                this.value = value;
            }

            /** @param high Upper nibble of an 8-bit byte
             *  @param low Lower nibble of an 8-bit byte
             * @return the byte represented by the nibbles. */
            public static byte toByte(Nibble high, Nibble low) {
                return (byte) ((high.value << 4) + (low.value));
            }
        }

        /** Character subtype for ·col·symbol⋯s.
         * Their code points are Unicode special characters */
        public static class Symbol extends Character {
            static void init() {}
            static Symbol exists(Symbol symbol) {
                Character.exists(symbol);
                return symbol;
            }

            public static final Symbol map = Symbol.exists(new Symbol((byte) 0x80, "→"));
            public static final Symbol yes = Symbol.exists(new Symbol((byte) 0x81, "✓"));
            public static final Symbol no = Symbol.exists(new Symbol((byte) 0x82, "✗"));
            public static final Symbol and = Symbol.exists(new Symbol((byte) 0x83, "∧"));
            public static final Symbol or = Symbol.exists(new Symbol((byte) 0x84, "∨"));
            public static final Symbol multiply = Symbol.exists(new Symbol((byte) 0x85, "∗"));
            public static final Symbol divide = Symbol.exists(new Symbol((byte) 0x86, "÷"));
            public static final Symbol query = Symbol.exists(new Symbol((byte) 0x87, "\uD83D\uDD0D"));

            protected Symbol(byte octet, String codePointString) {
                super(octet, codePointString);
            }
        }

        /** Character subtype for the ·col·separator⋯s. */
        public static class Separator extends Character {
            public Separator(byte octet, String codePointString) {
                super(octet, codePointString);
            }
        }

        /** Character subtype for the ·col·stop·character⋯. */
        public static class Stop extends Character {
            public Stop(byte octet, String codePointString) {
                super(octet, codePointString);
            }
        }

        /** The ·col·separator⋯. Used to separate ·col·token⋯s in serial ·c·o·l⋯. */
        public static final Separator separator = new Separator((byte) 0x10, "⋯");
        public static final Stop stop = new Stop((byte) 0x11, "⟩");
        static {
            exists(separator);
            exists(stop);
        }
    }

    public static abstract class Token {
        public abstract void toCharacters(Consumer<Character> destination);

        public static abstract class Name extends Token {
            public static class Constructed extends Name {
                private List<Character.Letter> letters;
                public Constructed(List<Character.Letter> letters) {
                    super();
                    this.letters = new ArrayList<>(letters);
                }

                public Constructed(String unicode) {
                    this(unicode.codePoints().boxed().map(c -> (Character.Letter) Character.ofCodePoint.get(c)).collect(Collectors.toList()));
                }

                @Override
                public String toString() {
                    return letters.stream().map(Character.Letter::toString).collect(Collectors.joining());
                }

                @Override
                public boolean equals(Object o) {
                    if (this == o) return true;
                    if (o == null || getClass() != o.getClass()) return false;
                    Constructed that = (Constructed) o;
                    return letters.equals(that.letters);
                }

                @Override
                public int hashCode() {
                    return letters.hashCode();
                }

                @Override
                public void toCharacters(Consumer<Character> destination) {
                    letters.forEach(destination);
                }
            }

            public static class Symbolic extends Name {
                private Character.Symbol symbol;
                public Symbolic(Character.Symbol symbol) {
                    super();
                    this.symbol = symbol;
                }

                @Override
                public String toString() {
                    return symbol.toString();
                }

                @Override
                public boolean equals(Object o) {
                    if (this == o) return true;
                    if (o == null || getClass() != o.getClass()) return false;
                    Symbolic symbolic = (Symbolic) o;
                    return symbol.equals(symbolic.symbol);
                }

                @Override
                public int hashCode() {
                    return symbol.hashCode();
                }

                @Override
                public void toCharacters(Consumer<Character> destination) {
                    destination.accept(symbol);
                }
            }

            public static Name fromLetters(Supplier<Character.Letter> source) {
                ArrayList<Character.Letter> letters = new ArrayList<>();
                for (Character.Letter letter = source.get(); letter != null; letter = source.get()) {
                    letters.add(letter);
                }
                return new Constructed(letters);
            }

            public static Name fromSymbols(Supplier<Character.Symbol> source) {
                Character.Symbol symbol = source.get();
                if (source.get() != null) throw new IllegalArgumentException("Invalid token characters");
                return new Symbolic(symbol);
            }
        }

        public static class Number extends Token {
            private int value;

            public Number(int value) {
                this.value = value;
            }

            public static Number fromDigits(Supplier<Character.Digit> source) {
                int value = 0;
                for (Character.Digit digit = source.get(); digit != null; digit = source.get()) {
                    value *= 0x10;
                    value += digit.value;
                }
                return new Number(value);
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Number number = (Number) o;
                return value == number.value;
            }

            @Override
            public int hashCode() {
                return value;
            }

            @Override
            public void toCharacters(Consumer<Character> destination) {
                Arrays.stream(Integer.toHexString(value).split(""))
                        .map(s -> Character.Digit.ofValue.get(Integer.valueOf(s, 0x10)))
                        .forEach(destination);
            }
        }

        public static class Data extends Token {
            private byte[] data;

            public Data(byte[] data) {
                this.data = data;
            }

            private static byte[] constructFromHexString(String hex) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                for (int i = 0; i * 2 < hex.length(); i++) {
                    out.write(Integer.valueOf(hex.substring(i * 2, i * 2 + 2), 0x10).byteValue());
                }
                return out.toByteArray();
            }

            public Data(String hexString) {
                this(constructFromHexString(hexString));
            }

            public static Data fromNibbles(Supplier<Character.Nibble> source) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                boolean highNibble = true;
                int octet = 0;
                for (Character.Nibble nibble = source.get(); nibble != null; nibble = source.get()) {
                    if (highNibble) {
                        octet = nibble.value << 4;
                    } else {
                        octet += nibble.value;
                        out.write(octet);
                    }
                    highNibble = !highNibble;
                }
                if (!highNibble) out.write(octet);
                return new Data(out.toByteArray());
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Data data1 = (Data) o;
                return Arrays.equals(data, data1.data);
            }

            @Override
            public int hashCode() {
                return Arrays.hashCode(data);
            }

            @Override
            public void toCharacters(Consumer<Character> destination) {
                for (byte b : data) {
                    destination.accept(Character.Nibble.ofValue.get((b & 0xFF) >> 4));
                    destination.accept(Character.Nibble.ofValue.get((b & 0xFF) & 0b1111));
                }
            }
        }

        public static class Stop extends Token {
            public static Stop fromStops(Supplier<Character.Stop> source) {
                source.get();
                if (source.get() != null) throw new IllegalArgumentException("Invalid token characters");
                return stop;
            }

            @Override
            public void toCharacters(Consumer<Character> destination) {
                destination.accept(Character.stop);
            }
        }

        public static final Stop stop = new Stop();

        private static Token fromCharacters(Supplier<Character> source, Character first) {
            try {
                if (first instanceof Character.Letter) return Name.fromLetters(() -> (Character.Letter) source.get());
                else if (first instanceof Character.Symbol) return Name.fromSymbols(() -> (Character.Symbol) source.get());
                else if (first instanceof Character.Digit) return Number.fromDigits(() -> (Character.Digit) source.get());
                else if (first instanceof Character.Nibble) return Data.fromNibbles(() -> (Character.Nibble) source.get());
                else if (first instanceof Character.Stop) return Stop.fromStops(() -> (Character.Stop) source.get());
            } catch (ClassCastException ignored) {}
            throw new IllegalArgumentException("Invalid token characters");
        }

        public static Token fromCharacters(Supplier<Character> source) {
            AtomicBoolean isFirst = new AtomicBoolean(true);
            Character first = source.get();
            return fromCharacters(() -> {
                if (isFirst.get()) {
                    isFirst.set(false);
                    return first;
                } else {
                    return source.get();
                }
            }, first);
        }
    }

    public static abstract class Expression extends AbstractList<Expression> {
        public abstract void toTokens(Consumer<Token> destination);
        public void toCharacters(Consumer<Character> destination) {
            toTokens(token -> {
                token.toCharacters(destination);
                destination.accept(Character.separator);
            });
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            toCharacters(c -> sb.appendCodePoint(c.codePoint));
            return sb.toString();
        }

        public void toOctets(Consumer<Byte> destination) {
            toCharacters(c -> destination.accept(c.octet));
        }

        @Override
        public Expression get(int index) {
            return null;
        }
        @Override
        public int size() {
            return 0;
        }

        public static class Number extends Expression {
            private Token.Number number;
            public Number(Token.Number number) {
                this.number = number;
            }
            public Number(int value) {
                this(new Token.Number(value));
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                if (!super.equals(o)) return false;
                Number that = (Number) o;
                return number.equals(that.number);
            }

            @Override
            public int hashCode() {
                int result = super.hashCode();
                result = 31 * result + number.hashCode();
                return result;
            }

            @Override
            public void toTokens(Consumer<Token> destination) {
                destination.accept(number);
            }

            @Override
            public String toString() {
                return super.toString();
            }
        }

        public static class Data extends Expression {
            private Token.Data data;
            public Data(Token.Data data) {
                this.data = data;
            }
            public Data(byte[] data) {
                this(new Token.Data(data));
            }
            public Data(String hexString) {
                this(new Token.Data(hexString));
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                if (!super.equals(o)) return false;
                Data that = (Data) o;
                return data.equals(that.data);
            }

            @Override
            public int hashCode() {
                int result = super.hashCode();
                result = 31 * result + data.hashCode();
                return result;
            }

            @Override
            public void toTokens(Consumer<Token> destination) {
                destination.accept(data);
            }

            @Override
            public String toString() {
                return super.toString();
            }
        }

        public static class Structure extends Expression {
            private Token.Name operator;
            private List<Expression> elements;

            public Structure(Token.Name operator, List<Expression> elements) {
                this.operator = operator;
                this.elements = new ArrayList<>(elements);
            }
            public Structure(Character.Symbol operatorSymbol, List<Expression> elements) {
                this(new Token.Name.Symbolic(operatorSymbol), elements);
            }
            public Structure(String operatorName, List<Expression> elements) {
                this(new Token.Name.Constructed(operatorName), elements);
            }
            public Structure(Character.Symbol operatorSymbol, Expression... elements) {
                this(operatorSymbol, Arrays.asList(elements));
            }
            public Structure(String operatorName, Expression... elements) {
                this(operatorName, Arrays.asList(elements));
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                if (!super.equals(o)) return false;
                Structure that = (Structure) o;
                return operator.equals(that.operator) && elements.equals(that.elements);
            }

            @Override
            public int hashCode() {
                int result = super.hashCode();
                result = 31 * result + operator.hashCode();
                result = 31 * result + elements.hashCode();
                return result;
            }

            public static Structure fromNameAndExpressions(Token.Name name, Supplier<Expression> source) {
                ArrayList<Expression> elements = new ArrayList<>();
                for (Expression element = source.get(); element != null; element = source.get()) {
                    elements.add(element);
                }
                return new Structure(name, elements);
            }

            @Override
            public void toTokens(Consumer<Token> destination) {
                destination.accept(operator);
                elements.forEach(e -> e.toTokens(destination));
                destination.accept(Token.stop);
            }

            @Override
            public Expression get(int index) {
                return elements.get(index);
            }

            @Override
            public int size() {
                return elements.size();
            }

            @Override
            public void add(int index, Expression element) {
                elements.add(index, element);
            }

            @Override
            public Expression set(int index, Expression element) {
                return elements.set(index, element);
            }

            @Override
            public Expression remove(int index) {
                return elements.remove(index);
            }

            @Override
            public String toString() {
                return super.toString();
            }
        }

        public static Expression fromTokens(Supplier<Token> source, boolean nullOnStop) {
            Token first = source.get();
            if (first instanceof Token.Number) return new Number((Token.Number) first);
            else if (first instanceof Token.Data) return new Data((Token.Data) first);
            else if (nullOnStop && first instanceof Token.Stop) return null;
            else if (first instanceof Token.Name) return Structure.fromNameAndExpressions((Token.Name) first,
                    () -> fromTokens(source, true));
            else if (first == null) throw new IllegalArgumentException("Missing token");
            throw new IllegalArgumentException("Unexpected token");
        }

        public static Expression fromCharacters(Supplier<Character> source) {
            return fromTokens(() -> Token.fromCharacters(() -> {
                Character c = source.get();
                if (c instanceof Character.Separator) return null;
                else return c;
            }), false);
        }

        public static Expression fromString(String unicode) {
            AtomicInteger index = new AtomicInteger(0);
            List<Integer> codePoints = unicode.codePoints().boxed().collect(Collectors.toList());
            return fromCharacters(() -> index.get() < codePoints.size() ? Character.ofCodePoint.get(codePoints.get(index.getAndIncrement())) : null);
        }

        public static Expression fromOctets(Supplier<Byte> source) {
            return fromCharacters(() -> Character.ofOctet.get(source.get()));
        }
    }
}
