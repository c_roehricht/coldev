package work.connor.col2;

import java.util.concurrent.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Utility {
   public static <I,T,O> O writeAndParse(I input, Function<Supplier<T>,O> parser, BiConsumer<I,Consumer<T>> writer) {
       BlockingQueue<T> transport = new LinkedBlockingQueue<T>();
       ExecutorService service =  Executors.newSingleThreadExecutor();
       Future<O> future = service.submit(() -> parser.apply(() -> {
           try {
               return transport.take();
           } catch (InterruptedException e) { // Thank you, Java threading designers
               throw new RuntimeException(e);
           }}));
       writer.accept(input, t -> {
           try {
               transport.put(t);
           } catch (InterruptedException e) {
               throw new RuntimeException(e);
           }
       });
       try {
           return future.get();
       } catch (InterruptedException | ExecutionException e) {
           throw new RuntimeException(e);
       }
   }

    public static <T1,O,T2> void parseAndWrite(Supplier<T1> source, Consumer<T2> destination, Function<Supplier<T1>,O> parser, BiConsumer<O,Consumer<T2>> writer) {
       writer.accept(parser.apply(source), destination);
    }
}
