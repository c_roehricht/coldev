package work.connor.col;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * @// TODO: 12.08.17 Add invariants to setters, general error checks
 * @// TODO: 16.08.17 Numbers shall not have leading zeroes (canonical form) 
 * @// TODO: 16.08.17 No empty lists
 */
public class COL {
    public static class Character {
        public enum Type {
            Digit, Letter, Entity, ListSeparator, Operator, ExpressionBegin, ExpressionEnd, DataBegin, DataEnd;
        }

        public static Map<Byte, Character> ofOctet = new HashMap<>();
        public static Map<Integer, Character> ofCodePoint = new HashMap<>();
        public final String name;
        public final int codePoint;
        public final byte octet;
        public final Type type;
        private Character(String name, String codePointString, byte octet, Type type) {
            this.name = name;
            this.codePoint = codePointString.codePoints().findFirst().getAsInt();
            this.octet = octet;
            this.type = type;
            ofOctet.put(octet, this);
            ofCodePoint.put(codePoint, this);
        }

        public static boolean octetHasType(byte octet, Type type) {
            Character c = ofOctet.get(octet);
            if (c != null) return c.type == type;
            else return false;
        }

        public static final Character DigitZero   = new Character("digitZero"   ,"0", (byte) 0x00, Type.Digit);
        public static final Character DigitOne    = new Character("digitOne"    ,"1", (byte) 0x01, Type.Digit);
        public static final Character DigitTwo    = new Character("digitTwo"    ,"2", (byte) 0x02, Type.Digit);
        public static final Character DigitThree  = new Character("digitThree"  ,"3", (byte) 0x03, Type.Digit);
        public static final Character DigitFour   = new Character("digitFour"   ,"4", (byte) 0x04, Type.Digit);
        public static final Character DigitFive   = new Character("digitFive"   ,"5", (byte) 0x05, Type.Digit);
        public static final Character DigitSix    = new Character("digitSix"    ,"6", (byte) 0x06, Type.Digit);
        public static final Character DigitSeven  = new Character("digitSeven"  ,"7", (byte) 0x07, Type.Digit);
        public static final Character DigitEight  = new Character("digitEight"  ,"8", (byte) 0x08, Type.Digit);
        public static final Character DigitNine   = new Character("digitNine"   ,"9", (byte) 0x09, Type.Digit);
        public static final Character DigitA      = new Character("digitA"      ,"A", (byte) 0x0A, Type.Digit);
        public static final Character DigitB      = new Character("digitB"      ,"B", (byte) 0x0B, Type.Digit);
        public static final Character DigitC      = new Character("digitC"      ,"C", (byte) 0x0C, Type.Digit);
        public static final Character DigitD      = new Character("digitD"      ,"D", (byte) 0x0D, Type.Digit);
        public static final Character DigitE      = new Character("digitE"      ,"E", (byte) 0x0E, Type.Digit);
        public static final Character DigitF      = new Character("digitF"      ,"F", (byte) 0x0F, Type.Digit);
        public static final Character ListSeparator = new Character("listSeparator", "■", (byte) 0x10, Type.ListSeparator);
        public static final Character LetterA     = new Character("letterA"     ,"a", (byte) 0x20, Type.Letter);
        public static final Character LetterB     = new Character("letterB"     ,"b", (byte) 0x21, Type.Letter);
        public static final Character LetterC     = new Character("letterC"     ,"c", (byte) 0x22, Type.Letter);
        public static final Character LetterD     = new Character("letterD"     ,"d", (byte) 0x23, Type.Letter);
        public static final Character LetterE     = new Character("letterE"     ,"e", (byte) 0x24, Type.Letter);
        public static final Character LetterF     = new Character("letterF"     ,"f", (byte) 0x25, Type.Letter);
        public static final Character LetterG     = new Character("letterG"     ,"g", (byte) 0x26, Type.Letter);
        public static final Character LetterH     = new Character("letterH"     ,"h", (byte) 0x27, Type.Letter);
        public static final Character LetterI     = new Character("letterI"     ,"i", (byte) 0x28, Type.Letter);
        public static final Character LetterJ     = new Character("letterJ"     ,"j", (byte) 0x29, Type.Letter);
        public static final Character LetterK     = new Character("letterK"     ,"k", (byte) 0x2A, Type.Letter);
        public static final Character LetterL     = new Character("letterL"     ,"l", (byte) 0x2B, Type.Letter);
        public static final Character LetterM     = new Character("letterM"     ,"m", (byte) 0x2C, Type.Letter);
        public static final Character LetterN     = new Character("letterN"     ,"n", (byte) 0x2D, Type.Letter);
        public static final Character LetterO     = new Character("letterO"     ,"o", (byte) 0x2E, Type.Letter);
        public static final Character LetterP     = new Character("letterP"     ,"p", (byte) 0x2F, Type.Letter);
        public static final Character LetterQ     = new Character("letterQ"     ,"q", (byte) 0x30, Type.Letter);
        public static final Character LetterR     = new Character("letterR"     ,"r", (byte) 0x31, Type.Letter);
        public static final Character LetterS     = new Character("letterS"     ,"s", (byte) 0x32, Type.Letter);
        public static final Character LetterT     = new Character("letterT"     ,"t", (byte) 0x33, Type.Letter);
        public static final Character LetterU     = new Character("letterU"     ,"u", (byte) 0x34, Type.Letter);
        public static final Character LetterV     = new Character("letterV"     ,"v", (byte) 0x35, Type.Letter);
        public static final Character LetterW     = new Character("letterW"     ,"w", (byte) 0x36, Type.Letter);
        public static final Character LetterX     = new Character("letterX"     ,"x", (byte) 0x37, Type.Letter);
        public static final Character LetterY     = new Character("letterY"     ,"y", (byte) 0x38, Type.Letter);
        public static final Character LetterZ     = new Character("letterZ"     ,"z", (byte) 0x39, Type.Letter);
        public static final Character LetterAX    = new Character("letterAX"    ,"ā", (byte) 0x3A, Type.Letter);
        public static final Character LetterEX    = new Character("letterEX"    ,"ē", (byte) 0x3B, Type.Letter);
        public static final Character LetterIX    = new Character("letterIX"    ,"ī", (byte) 0x3C, Type.Letter);
        public static final Character LetterOX    = new Character("letterOX"    ,"ō", (byte) 0x3D, Type.Letter);
        public static final Character LetterUX    = new Character("letterUX"    ,"ū", (byte) 0x3E, Type.Letter);
        public static final Character Syllabifier = new Character("syllabifier" ,"·", (byte) 0x3F, Type.Letter);
        public static final Character EntityTrue  = new Character("entityTrue"  ,"⊤", (byte) 0x40, Type.Entity);
        public static final Character EntityFalse = new Character("entityFalse" ,"⊥", (byte) 0x41, Type.Entity);
        public static final Character OperatorMap = new Character("operatorMap" ,"→", (byte) 0x80, Type.Operator);
        public static final Character ExpressionBegin = new Character("expressionBegin" ,"(", (byte) 0xFC, Type.ExpressionBegin);
        public static final Character ExpressionEnd   = new Character("expressionEnd"   ,")", (byte) 0xFD, Type.ExpressionEnd);
        public static final Character DataBegin       = new Character("dataBegin"       ,"⟨", (byte) 0xFE, Type.DataBegin);
        public static final Character DataEnd         = new Character("dataEnd"         ,"⟩", (byte) 0xFF, Type.DataEnd);

        public static int valueOfDigit(byte octet) {
            return (octet & 0xFF) - (DigitZero.octet & 0xFF);
        }
        public static byte digitForValue(int val) {
            return (byte) (val + (DigitZero.octet & 0xFF));
        }
        public static String unicodeFromOctets(byte[] octets) {
            StringBuilder sb = new StringBuilder();
            for (byte octet : octets) {
                Character c = ofOctet.get(octet);
                sb.appendCodePoint(c != null ? c.codePoint : (int) '?');
            }
            return sb.toString();
        }
        public static byte[] octetsFromUnicode(String unicode) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            unicode.codePoints().forEach(u -> out.write(ofCodePoint.get(u).octet));
            return out.toByteArray();
        }
    }

    public static abstract class Expression extends AbstractList<Expression> {
        @Override
        public Expression get(int index) {
            return null;
        }

        @Override
        public int size() {
            return 0;
        }

        public int descendants() {
            return stream().mapToInt(Expression::size).sum();
        }

        public abstract void write(ByteArrayOutputStream dest) throws IOException;

        /** Reads an expression from a buffer.
         * @param src The source buffer
         * @return the parsed expression. */
        public static Expression parse(ByteBuffer src) {
            int level = 0;
            int start = src.position();
            try {
                while (Character.octetHasType(src.get(src.position()), Character.Type.ExpressionBegin)) {
                    src.get();
                    level++;
                }
                Expression e;
                if (((e = Name.parse(src)) != null)
                 || ((e = Number.parse(src)) != null)
                 || ((e = Entity.parse(src)) != null)
                 || ((e = List.parse(src)) != null)
                 || ((e = Operation.parse(src)) != null)
                 || ((e = Data.parse(src)) != null)
                 || ((e = Null.parse(src)) != null)) {
                    while (level > 0
                        && Character.octetHasType(src.get(src.position()), Character.Type.ExpressionEnd)) {
                        src.get();
                        level--;
                    }
                    if (level == 0) return e;
                }
            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {}
            src.position(start);
            return Null.parse(src);
        }

        @Override
        public String toString() {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {write(out);} catch (IOException ex) {}
            return Character.unicodeFromOctets(out.toByteArray());
        }
    }

    public static class Name extends Expression {
        private byte[] name;

        private Name() {
        }

        public Name(byte[] name) {
            setName(name);
        }

        private static byte[] constructFromUnicode(String unicode) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            unicode.codePoints().forEach(u -> {
                Character c = Character.ofCodePoint.get(u);
                if (c.type == Character.Type.Letter) out.write(c.octet);
                else throw new IllegalArgumentException();
            });
            return out.toByteArray();
        }

        public Name(String unicode) {
            this(constructFromUnicode(unicode));
        }

        public byte[] getName() {
            return name;
        }

        public void setName(byte[] name) {
            this.name = name;
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException {
            dest.write(Character.Syllabifier.octet);
            dest.write(name);
        }

        /** Reads a col-name from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-name, or null if parsing failed. */
        public static Name parse(ByteBuffer src) {
            Name name = null;
            int start = src.position();
            ByteArrayOutputStream out = null;
            try {
                if (src.get(src.position()) == Character.Syllabifier.octet) {
                    src.get();
                    out = new ByteArrayOutputStream();
                    for (byte octet = src.get(src.position()); Character.octetHasType(octet, Character.Type.Letter); octet = src.get(src.position())) {
                        src.get();
                        out.write(octet);
                    }
                }
            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {}
            if (out != null) {
                return new Name(out.toByteArray());
            } else {
                src.position(start);
                return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            Name that = (Name) o;
            return Arrays.equals(name, that.name);
        }
        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + Arrays.hashCode(name);
            return result;
        }
    }

    public static class Number extends Expression {
        private Integer number;

        private Number() {
        }

        public Number(Integer number) {
            setNumber(number);
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException {
            String hex = Integer.toHexString(number);
            for (int i = 0; i < hex.length(); i++) {
                dest.write(Character.digitForValue(Integer.valueOf(hex.substring(i, i+1), 0x10)));
            }
        }

        /** Reads a col-number from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-number, or null if parsing failed. */
        public static Number parse(ByteBuffer src) {
            Name name = null;
            int start = src.position();
            Integer value = null;
            try {
                if (Character.octetHasType(src.get(src.position()), Character.Type.Digit)) {
                    value = 0;
                    for (byte octet = src.get(src.position()); Character.octetHasType(octet, Character.Type.Digit); octet = src.get(src.position())) {
                        src.get();
                        value *= 0x10;
                        value += Character.valueOfDigit(octet);
                    }
                }
            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {}
            if (value != null) {
                return new Number(value);
            } else {
                src.position(start);
                return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            Number that = (Number) o;
            return number.equals(that.number);
        }
        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + number.hashCode();
            return result;
        }
    }

    public static class Null extends Expression {
        public Null() {
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException {
        }

        /** Reads a col-null from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-null, or null if parsing failed. */
        public static Null parse(ByteBuffer src) {
            return new Null();
        }
    }

    public static class Entity extends Expression {
        private byte entity;

        public Entity(byte entity) {
            setEntity(entity);
        }

        public Entity(Character entity) {
            this(entity.octet);
        }

        public byte getEntity() {
            return entity;
        }

        public void setEntity(byte entity) {
            if (!Character.octetHasType(entity, Character.Type.Entity)) throw new IllegalArgumentException();
            this.entity = entity;
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException {
            dest.write(entity);
        }

        /** Reads a col-entity from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-entity, or null if parsing failed. */
        public static Entity parse(ByteBuffer src) {
            int pos = src.position();
            try {
                return new Entity(src.get());
            } catch (IllegalArgumentException|BufferUnderflowException|IndexOutOfBoundsException ex) {
                src.position(pos);
                return null;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            Entity that = (Entity) o;
            return entity == that.entity;
        }
        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + (int) entity;
            return result;
        }
    }

    public static class List extends Expression {
        private ArrayList<Expression> list;

        private List() {
        }

        public List(java.util.List<Expression> list) {
            this.list = new ArrayList<>(list);
        }

        public List(Expression... expressions) {
            this(Arrays.asList(expressions));
        }

        @Override
        public Expression get(int index) {
            return list.get(index);
        }

        @Override
        public int size() {
            return list.size();
        }

        @Override
        public boolean add(Expression expression) {
            return list.add(expression);
        }

        @Override
        public Expression remove(int index) {
            return list.remove(index);
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException {
            for (Expression e : this) {
                dest.write(Character.ListSeparator.octet);
                if (e instanceof List || e instanceof Operation || e instanceof Null) {
                    dest.write(Character.ExpressionBegin.octet);
                }
                e.write(dest);
                if (e instanceof List || e instanceof Operation || e instanceof Null) {
                    dest.write(Character.ExpressionEnd.octet);
                }
            }
        }

        /** Reads a col-list from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-list, or null if parsing failed. */
        public static List parse(ByteBuffer src) {
            java.util.List<Expression> elements = null;
            int start = src.position();
            try {
                if (Character.octetHasType(src.get(src.position()), Character.Type.ListSeparator)) {
                    elements = new ArrayList<>();
                    for (byte octet = src.get(src.position()); Character.octetHasType(octet, Character.Type.ListSeparator); octet = src.get(src.position())) {
                        src.get();
                        elements.add(Expression.parse(src));
                    }
                }
            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {}
            if (elements != null) {
                return new List(elements);
            } else {
                src.position(start);
                return null;
            }
        }
    }

    public static class Operation extends Expression {
        private ArrayList<Expression> arguments;
        private byte operator;

        private Operation() {
        }

        public Operation(byte operator, java.util.List<Expression> arguments) {
            setOperator(operator);
            this.arguments = new ArrayList<>(arguments);
        }

        public Operation(Character operator, Expression... arguments) {
            this(operator.octet, Arrays.asList(arguments));
        }

        @Override
        public Expression get(int index) {
            return arguments.get(index);
        }

        @Override
        public int size() {
            return arguments.size();
        }

        @Override
        public boolean add(Expression expression) {
            return arguments.add(expression);
        }

        @Override
        public Expression remove(int index) {
            return arguments.remove(index);
        }

        public byte getOperator() {
            return operator;
        }

        public void setOperator(byte operator) {
            this.operator = operator;
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException {
            dest.write(operator);
            for (Expression e : this) {
                dest.write(Character.ListSeparator.octet);
                if (e instanceof List || e instanceof Operation || e instanceof Null) {
                    dest.write(Character.ExpressionBegin.octet);
                }
                e.write(dest);
                if (e instanceof List || e instanceof Operation || e instanceof Null) {
                    dest.write(Character.ExpressionEnd.octet);
                }
            }
        }

        /** Reads a col-operation from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-operation, or null if parsing failed. */
        public static Operation parse(ByteBuffer src) {
            try {
                if (Character.octetHasType(src.get(src.position()), Character.Type.Operator)) {
                    byte operator = src.get();
                    List arguments = List.parse(src);
                    if (arguments != null) {
                        return new Operation(operator, arguments);
                    } else {
                        Null.parse(src);
                        return new Operation(operator, Collections.emptyList());
                    }
                }
            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {}
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            Operation that = (Operation) o;
            return operator == that.operator;
        }
        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + (int) operator;
            return result;
        }
    }

    public static class Data extends Expression {
        private byte[] data;

        private Data() {
        }

        public Data(byte[] data) {
            setData(data);
        }

        private static byte[] constructFromHexString(String hex) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            for (int i = 0; i * 2 < hex.length(); i++) {
                out.write(Integer.valueOf(hex.substring(i * 2, i * 2 + 2), 0x10).byteValue());
            }
            return out.toByteArray();
        }

        public Data(String hex) {
            this(constructFromHexString(hex));
        }

        public byte[] getData() {
            return data;
        }

        public void setData(byte[] data) {
            this.data = data;
        }

        @Override
        public void write(ByteArrayOutputStream dest) throws IOException{
            dest.write(Character.DataBegin.octet);
            for (byte octet : data) {
                if (Character.octetHasType(octet, Character.Type.DataEnd)) dest.write(Character.DataEnd.octet);
                dest.write(octet);
            }
            dest.write(Character.DataEnd.octet);
        }

        /** Reads a col-data from a buffer. Only advances the buffer if parsing succeeded.
         * @param src The source buffer
         * @return the col-data, or null if parsing failed. */
        public static Data parse(ByteBuffer src) {
            int start = src.position();
            try {
                if (Character.octetHasType(src.get(src.position()), Character.Type.DataBegin)) {
                    src.get();
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    for (byte octet = src.get(); ; octet = src.get()) {
                        if (Character.octetHasType(octet, Character.Type.DataEnd)) {
                            try {
                                if (Character.octetHasType(src.get(src.position()), Character.Type.DataEnd)) {
                                    src.get();
                                } else {
                                    break;
                                }
                            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {
                                break;
                            }
                        }
                        out.write(octet);
                    }
                    return new Data(out.toByteArray());
                }
            } catch (BufferUnderflowException|IndexOutOfBoundsException ex) {}
            src.position(start);
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            Data that = (Data) o;
            return Arrays.equals(data, that.data);
        }
        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + Arrays.hashCode(data);
            return result;
        }
    }
}
